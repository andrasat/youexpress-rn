
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';

export class SmallCardIconOnRight extends React.Component {

	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.text}>{this.props.text}</Text>
				<TouchableOpacity onPress={() => alert('Demo !')}>
					<Ionicons name={this.props.iconName} size={30} style={styles.icon}/>
				</TouchableOpacity>
			</View>
		);
	}
}

SmallCardIconOnRight.propTypes = {
	iconName: PropTypes.string,
	text: PropTypes.string
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 10,
		margin: 15,
		backgroundColor: '#fff',
		flexDirection: 'row'
	},
	icon: {
		color: '#00008B',
		textAlign: 'right'
	},
	text: {
		color: '#000',
		alignSelf: 'center',
		textAlign: 'left',
		fontWeight: '100',
		fontSize: 17,
		flex: 1
	}
});