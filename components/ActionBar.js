
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, } from 'react-native';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';

export class ActionBar extends React.Component {

	render() {
		return (
			<View style={styles.container}>
				<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
					<Ionicons name="md-arrow-back" size={30} style={styles.icon}/>
				</TouchableOpacity>
				<Text style={styles.text}>{this.props.text}</Text>
			</View>
		);
	}
}

ActionBar.propTypes = {
	text: PropTypes.string,
};

const styles = StyleSheet.create({
	container: {
		paddingTop: 40,
		paddingLeft: 15,
		paddingRight: 15,
		paddingBottom: 15,
		backgroundColor: '#fff',
		flexDirection: 'row'
	},
	icon: {
		color: '#000',
		textAlign: 'left'
	},
	text: {
		alignSelf: 'center',
		color: '#000',
		textAlign: 'center',
		fontWeight: '600',
		fontSize: 20,
		flex: 1
	}
});