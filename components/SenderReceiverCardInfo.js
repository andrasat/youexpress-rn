
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, TextInput, } from 'react-native';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';
import Collapsible from 'react-native-collapsible';

export class SenderReceiverCardInfo extends React.Component {

	state = {
		collapsedOne: false,
		collapsedTwo: true,
		collapsedThree: true,
		defaultAddressOne: 'Jl. Jendral Sudirman no.1',
		defaultAddressTwo: 'Jl. Pamoyanan no.14',
		defaultAddressThree: 'Jl. A. Yani no.292',
		senderName: '',
		senderPhone: '',
		senderNote: '',
		receiverName: '',
		receiverPhone: '',
		receiverNote: '',
	}

	_setCollapseOne = () => {
		this.setState({ collapsedOne: !this.state.collapsedOne });
	}

	_setCollapsetwo = () => {
		this.setState({ collapsedTwo: !this.state.collapsedTwo });
	}

	_setCollapseThree = () => {
		this.setState({ collapsedTwo: !this.state.collapsedThree });
	}

	_setSenderName = (value) => {
		this.setState({ senderName: value });
	}

	_setSenderPhone = (value) => {
		this.setState({ senderPhone: value });	
	}

	_setSenderNote = (value) => {
		this.setState({ senderNote: value });	
	}

	_renderMiniSenderContactCard = () => {
		return (
			<View style={{ marginTop: 5 }}>

				<View style={{ flexDirection: 'row' }}>
					<View style={styles.smallColumn}>
						<Ionicons name="md-contact" size={30} style={{ color: '#00008B' }}/>
					</View>
					<View style={styles.bigColumn}>
						<TextInput
							style={{ fontSize: 16}}
							underlineColorAndroid="transparent"
							placeholderTextColor="#aeaeae"
							placeholder="Name"
							onChangeText={this._setSenderName}
						/>
					</View>
				</View>
				<View style={{ borderBottomWidth: 0.6, borderColor: '#929292', marginTop: 5, marginBottom: 5, }} />

				<View style={{ flexDirection: 'row' }}>
					<View style={styles.smallColumn}>
						<Ionicons name="md-call" size={30} style={{ color: '#00008B' }}/>
					</View>
					<View style={styles.bigColumn}>
						<TextInput
							style={{ fontSize: 16}}
							underlineColorAndroid="transparent"
							placeholderTextColor="#aeaeae"
							placeholder="Phone number"
							onChangeText={this._setSenderPhone}
						/>
					</View>
				</View>
				<View style={{ borderBottomWidth: 0.6, borderColor: '#929292', marginTop: 5, marginBottom: 5, }} />

				<View style={{ flexDirection: 'row' }}>
					<View style={styles.bigColumn}>
						<TextInput
							style={{ fontSize: 16}}
							underlineColorAndroid="transparent"
							placeholderTextColor="#aeaeae"
							placeholder="Notes, Instruction, or Location Detail"
							onChangeText={this._setSenderNote}
						/>
					</View>
				</View>
				<View style={{ borderBottomWidth: 0.6, borderColor: '#929292', marginTop: 5, marginBottom: 5, }} />

			</View>
		);
	}

	_setReceiverName = (value) => {
		this.setState({ receiverName: value });
	}

	_setReceiverPhone = (value) => {
		this.setState({ receiverPhone: value });
	}

	_setReceiverNote = (value) => {
		this.setState({ receiverNote: value });	
	}

	_renderMiniReceiverContactCard = () => {
		return (
			<View style={{ marginTop: 5 }}>

				<View style={{ flexDirection: 'row' }}>
					<View style={styles.smallColumn}>
						<Ionicons name="md-contact" size={30} style={{ color: '#ff9a00' }}/>
					</View>
					<View style={styles.bigColumn}>
						<TextInput
							style={{ fontSize: 16}}
							underlineColorAndroid="transparent"
							placeholderTextColor="#aeaeae"
							placeholder="Name"
							onChangeText={this._setReceiverName}
						/>
					</View>
				</View>
				<View style={{ borderBottomWidth: 0.6, borderColor: '#929292', marginTop: 10, marginBottom: 5, }} />

				<View style={{ flexDirection: 'row' }}>
					<View style={styles.smallColumn}>
						<Ionicons name="md-call" size={30} style={{ color: '#ff9a00' }}/>
					</View>
					<View style={styles.bigColumn}>
						<TextInput
							style={{ fontSize: 16}}
							underlineColorAndroid="transparent"
							placeholderTextColor="#aeaeae"
							placeholder="Phone number"
							onChangeText={this._setReceiverPhone}
						/>
					</View>
				</View>
				<View style={{ borderBottomWidth: 0.6, borderColor: '#929292', marginTop: 10, marginBottom: 5, }} />

				<View style={{ flexDirection: 'row' }}>
					<View style={styles.bigColumn}>
						<TextInput
							style={{ fontSize: 16}}
							underlineColorAndroid="transparent"
							placeholderTextColor="#aeaeae"
							placeholder="Notes, Instruction, or Location Detail"
							onChangeText={this._setReceiverNote}
						/>
					</View>
				</View>
				<View style={{ borderBottomWidth: 0.6, borderColor: '#929292', marginTop: 10, marginBottom: 5, }} />

			</View>
		);
	}

	render() {
		return (
			<View style={styles.bigContainer}>

				<Text style={styles.text}>
					Enter contact number (sender and receiver) and note for the driver
				</Text>

				<View style={styles.smallContainer}>

					<View style={styles.smallColumn}>
						<Ionicons name="ios-pin" size={40} style={icons.senderPin}/>
					</View>

					<View style={styles.bigColumn}>
						<View style={{flexDirection: 'row'}}>
							<View style={{flex: 1}}>
								<Text style={texts.senderTitle}>Sender</Text>
							</View>
							<TouchableOpacity onPress={this._setCollapseOne}>
								<Ionicons name={this.state.collapsedOne ? "ios-arrow-down" : "ios-arrow-up"} size={25} style={icons.arrowDown}/>
							</TouchableOpacity>
						</View>
						<View>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('InputAddress')}>
								<Text style={texts.addresses}>{this.state.defaultAddressOne}</Text>
							</TouchableOpacity>
						</View>
						<Collapsible collapsed={this.state.collapsedOne} align="center">
							{this._renderMiniSenderContactCard()}
						</Collapsible>
					</View>

				</View>

				<View style={styles.smallContainer}>

					<View style={styles.smallColumn}>
						<Ionicons name="ios-pin" size={40} style={icons.receiverPin}/>
					</View>

					<View style={styles.bigColumn}>
						<View style={{flexDirection: 'row'}}>
							<View style={{flex: 1}}>
								<Text style={texts.receiverTitle}>Receiver 1</Text>
							</View>
							<TouchableOpacity onPress={this._setCollapsetwo}>
								<Ionicons name={this.state.collapsedTwo ? "ios-arrow-down" : "ios-arrow-up"} size={25} style={icons.arrowDown}/>
							</TouchableOpacity>
						</View>
						<View>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('InputAddress')}>
								<Text style={texts.addresses}>{this.state.defaultAddressTwo}</Text>
							</TouchableOpacity>
						</View>
						<Collapsible collapsed={this.state.collapsedTwo} align="center">
							{this._renderMiniReceiverContactCard()}
						</Collapsible>
					</View>

				</View>

				<View style={styles.smallContainer}>

					<View style={styles.smallColumn}>
						<Ionicons name="ios-pin" size={40} style={icons.receiverPin}/>
					</View>

					<View style={styles.bigColumn}>
						<View style={{flexDirection: 'row'}}>
							<View style={{flex: 1}}>
								<Text style={texts.receiverTitle}>Receiver 2</Text>
							</View>
							<TouchableOpacity onPress={this._setCollapseThree}>
								<Ionicons name={this.state.collapsedThree ? "ios-arrow-down" : "ios-arrow-up"} size={25} style={icons.arrowDown}/>
							</TouchableOpacity>
						</View>
						<View>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('InputAddress')}>
								<Text style={texts.addresses}>{this.state.defaultAddressThree}</Text>
							</TouchableOpacity>
						</View>
						<Collapsible collapsed={this.state.collapsedThree} align="center">
							{this._renderMiniReceiverContactCard()}
						</Collapsible>
					</View>

				</View>

			</View>
		);
	}
}

SenderReceiverCardInfo.propTypes = {

};

const icons = StyleSheet.create({
	senderPin: {
		margin: 2,
		color: '#00008B',
	},
	receiverPin: {
		margin: 2,
		color: '#ff9a00',
	},
	arrowDown: {
		color: '#aeaeae',
		textAlign: 'right',
	},
});

const texts = StyleSheet.create({
	senderTitle: {
		color: '#00008B',
		textAlign: 'left',
		fontWeight: '700',
		fontSize: 25,
	},
	receiverTitle: {
		color: '#ff9a00',
		textAlign: 'left',
		fontWeight: '700',
		fontSize: 25,
	},
	addresses: {
		color: '#aeaeae'
	},
})

const styles = StyleSheet.create({
	bigContainer: {
		flex: 9,
		padding: 10,
		marginTop: 0,
		marginLeft: 15,
		marginRight: 15,
		backgroundColor: '#fff',
	},
	smallContainer: {
		flexDirection: 'row',
		borderBottomWidth: 0.4,
		padding: 5,
		marginTop: 6,
		paddingBottom: 10,
		backgroundColor: '#fff',
	},
	miniContainer: {
		flex: 1,
		margin: 1,
		backgroundColor: '#fff'
	},
	smallColumn: {
		flex: 0.1,
		marginTop: 5,
	},
	bigColumn: {
		flex: 0.9,
		marginTop: 5,
	},
	text: {
		color: '#000',
		textAlign: 'left',
		fontWeight: '100',
		fontSize: 17,
		flex: 1
	}
});