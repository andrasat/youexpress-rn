
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Dimensions, } from 'react-native';
import { Button } from 'react-native-elements';

import { Ionicons, Entypo, FontAwesome, } from '@expo/vector-icons';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';

const { height, width } = Dimensions.get('window');

export class BottomCard extends React.Component {

	render() {
		return (
			<View style={styles.container}>

				<View style={styles.card}>
					
					<View style={styles.priceContainer}>
						<View style={{ flexDirection: 'row' }}>
							<Text style={styles.priceText}>Price</Text>
							<Text style={styles.priceNumberText}>IDR 200.000</Text>
						</View>
						<View style={{ flexDirection: 'row' }}>
							<Text style={styles.priceText}>Door to door (driver)</Text>
							<Text style={styles.priceNumberText}>IDR 50.000</Text>
						</View>
					</View>

					<View style={styles.priceTotalContainer}>
						<Text style={styles.priceTotalText}>Total Payment</Text>
						<Text style={styles.priceNumberTotalText}>IDR 250.000</Text>
					</View>

				</View>

				<View style={styles.card}>

					<Text style={styles.paymentText}>Responsible for payment</Text>
					
					<RadioGroup
						style={{ flexDirection: 'row' }}
						size={27}
						thickness={3}
						selectedIndex={0}
						color="#00008B"
					>

						<RadioButton>
							<Text>Sender</Text>
						</RadioButton>

						<RadioButton>
							<Text>Sender</Text>
						</RadioButton>

					</RadioGroup>

				</View>

				<View style={styles.card}>

					<Text style={styles.paymentText}>Pay with</Text>

					<View style={styles.iconChooserContainer}>

						<View style={styles.boxChooser}>
							<Ionicons name="ios-cash-outline" size={40} style={{ marginBottom: 5 }} />
							<Text style={{ textAlign: 'center' }}>Cash</Text>
						</View>

						<View style={styles.boxChooser}>
							<Entypo name="wallet" size={40} style={{ marginBottom: 5 }} />
							<Text style={{ textAlign: 'center' }}>Wallet</Text>
						</View>

						<View style={styles.boxChooser}>
							<FontAwesome name="credit-card" size={40} style={{ marginBottom: 5 }} />
							<Text style={{ textAlign: 'center' }}>Billed</Text>
						</View>
						
					</View>

					<Button
						onPress={() => alert('Order Confirmed !')}
						activeOpacity={0.9}
						TouchableComponent="TouchableOpacity"
						title="Order"
						titleStyle={{
							fontWeight: '500',
							color: '#fff'
						}}
						buttonStyle={{
							backgroundColor: '#00008B',
							borderRadius: 10,
						}}
					/>
				</View>				

			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		height: height * 0.75,
		flex: 1,
		backgroundColor: '#fff',
		marginTop: 15,
	},
	card: {
		borderBottomWidth: 0.5,
		borderBottomColor: '#929292',
		padding: 25,
	},
	iconChooserContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignContent: 'space-between',
		marginBottom: 15,
	},
	boxChooser: {
		marginLeft: 15,
		marginRight: 15,
		height: width / 4.5,
		width: width / 4.5,
		borderWidth: 0.5,
		borderColor: '#000',
		borderRadius: 5,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},
	paymentText: {
		marginBottom: 15,
		fontSize: 20,
		fontWeight: '500',
		color: '#000',
	},
	priceContainer: {
		borderBottomWidth: 0.5,
		borderBottomColor: '#929292',
		paddingBottom: 15,
		alignItems: 'center',
	},
	priceTotalContainer: {
		flexDirection: 'row',
		marginTop: 15,
	},
	priceText: {
		flex: 0.6,
		paddingBottom: 5,
		textAlign: 'left',
		fontSize: 20,
		color: '#000'
	},
	priceNumberText: {
		flex: 0.4,
		paddingBottom: 5,
		textAlign: 'right',
		fontSize: 20,
		color: '#000'
	},
	priceTotalText: {
		flex: 0.6,
		paddingBottom: 5,
		textAlign: 'left',
		fontSize: 20,
		color: '#00008B',
		fontWeight: '600',
	},
	priceNumberTotalText: {
		flex: 0.4,
		paddingBottom: 5,
		textAlign: 'right',
		fontSize: 25,
		color: '#00008B',
		fontWeight: '600',
	},
});