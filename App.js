
import React from 'react';
import { AppLoading, Font } from 'expo';
import { StackNavigator } from 'react-navigation';

import HomeScreen from './screens/HomeScreen';
import AddAddressScreen from './screens/AddAddressScreen';

const RootNavigator = StackNavigator({
	Home: {
		screen: HomeScreen
	},
	InputAddress: {
		screen: AddAddressScreen
	},
}, {
	initialRouteName: 'Home',
	headerMode: 'none',
});

export default class App extends React.Component {

	state = {
		asyncAssetsLoaded: false
	}

	render() {

		if(!this.state.asyncAssetsLoaded) {

			return (
				<AppLoading
					startAsync={this._loadResources}
					onError={this._handleOnError}
					onFinish={this._handleFinish}
				/>
			);

		} else {

			return (
				<RootNavigator />
			);
		}
	}

	_loadResources = async () => {

		await Font.loadAsync({
			'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf')
		});
	}

	_handleOnError = (err) => {
		console.error(err);
	}

	_handleFinish = () => {
		this.setState({asyncAssetsLoaded: true});
	}
}
