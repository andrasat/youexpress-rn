
import React from 'react';
import {
	Image,
	Platform,
	ScrollView,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
	StatusBar,
} from 'react-native';

import { MonoText } from '../components/StyledText';
import { ActionBar } from '../components/ActionBar';
import { SmallCardIconOnRight } from '../components/SmallCardIconOnRight';
import { SenderReceiverCardInfo } from '../components/SenderReceiverCardInfo';
import { BottomCard } from '../components/BottomCard';

export default class HomeScreen extends React.Component {

	static navigationOptions = {
  		title: null,
  	};

	render() {
		return (
			<View style={styles.container}>

				<StatusBar barStyle="light-content"/>

				<ActionBar text="Confirm Order" navigation={this.props.navigation}/>

				<ScrollView style={styles.greyContainer} contentContainerStyle={styles.contentContainer}>

					<SmallCardIconOnRight text="Your Package Photo" iconName="ios-add-circle" />

					<SenderReceiverCardInfo navigation={this.props.navigation}/>

					<BottomCard />

				</ScrollView>

			</View>
		);
	}

}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	greyContainer: {
		flex: 1,
		backgroundColor: '#D3D3D3',
	},
	contentContainer: {
		paddingTop: 0,
	},	
});
