
import React from 'react';
import { ScrollView, StyleSheet, View, StatusBar, } from 'react-native';
import { ActionBar } from '../components/ActionBar';

export default class AddAddressScreen extends React.Component {
	static navigationOptions = {
	    title: null,
	  };

	render() {
		return (
			<View style={styles.container}>

				<StatusBar barStyle="light-content"/>

				<ActionBar text="Pick Up Location" navigation={this.props.navigation}/>
				
				<ScrollView style={styles.container}>

				</ScrollView>

			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	greyContainer: {
		flex: 1,
		backgroundColor: '#D3D3D3',
	},
});
